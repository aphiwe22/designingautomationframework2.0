﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace EAAutoFramework.Base
{
    public abstract class BasePage : Base
    {
        private IWebDriver _driver {get; set; }


        public BasePage()
        {
            PageFactory.InitElements(DriverContext.Driver, this);
        }
       
    }

}
